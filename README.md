# Microvolts
is an open source oscilloscope project made to support beginner electronics as well as make more experienced peoples' life easier without spending lots of money for professional hardware.

This repo contains:
- Programs for both PC and AVR side
- Official and approved schematics and PCB layouts
- Scripts which task is to make your experience as smooth as possible
